import '../styles/test1.css';
import '../styles/test2.scss';
import '../styles/test3.less';
import React from 'react';
import ReactDOM from 'react-dom';
// import logo from '../statics/images/logo.svg';
import logo from '../statics/images/test.jpg';


async function asyncFunc() {
  return 123;
}

asyncFunc()
  .then(x => console.log(x));


const App = () => (
  <div className="App">
    <img className="App-Logo" src={logo} alt="React Logo" />
    <h1 className="App-Title">Hello Parcel  React</h1>
  </div>
);

ReactDOM.render(<App />, document.getElementById('root'));

// Hot Module Replacement
if (module.hot) {
  module.hot.accept();
}